<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <source>About BibSearch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version 0.1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BibliographyListView</name>
    <message>
        <source>New Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>BibSearch</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EntryView</name>
    <message>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Publication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>BibTeX Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View Online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DOI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>BibTeX Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchForm</name>
    <message>
        <source>Start Searching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Free text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Searching the following services: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
