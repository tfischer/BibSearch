#!/usr/bin/env dash

rsvg-convert -f png -w 86 -h 86 -o harbour-bibsearch.png harbour-bibsearch.svg && optipng harbour-bibsearch.png
for size in 108 128 256 ; do
	pngfile=icons/${size}/harbour-bibsearch.png
	rsvg-convert -f png -w ${size} -h ${size} -o ${pngfile} harbour-bibsearch.svg && optipng ${pngfile}
done
