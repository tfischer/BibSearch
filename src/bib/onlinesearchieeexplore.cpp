/***************************************************************************
 *   Copyright (C) 2004-2017 by Thomas Fischer <fischer@unix-ag.uni-kl.de> *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "onlinesearchieeexplore.h"

#include <QNetworkReply>
#include <QStandardPaths>
#include <QUrl>
#include <QUrlQuery>
#include <QCoreApplication>

#ifdef HAVE_KF5
#include <KLocalizedString>
#endif // HAVE_KF5

#include "internalnetworkaccessmanager.h"
#include "xsltransform.h"
#include "encoderxml.h"
#include "fileimporterbibtex.h"

class OnlineSearchIEEEXplore::OnlineSearchIEEEXplorePrivate
{
public:
    static const QUrl apiUrl;
    const XSLTransform xslt;

    OnlineSearchIEEEXplorePrivate(OnlineSearchIEEEXplore *)
            : xslt(QStandardPaths::locate(QStandardPaths::GenericDataLocation, QCoreApplication::instance()->applicationName().remove(QStringLiteral("test")) + QStringLiteral("/ieeexploreapiv1-to-bibtex.xsl")))
    {
        /// nothing
    }

    QUrl buildQueryUrl(const QMap<QString, QString> &query, int numResults) {
        QUrl queryUrl = apiUrl;
        QUrlQuery q(queryUrl.query());

        /// Free text
        const QStringList freeTextFragments = OnlineSearchAbstract::splitRespectingQuotationMarks(query[queryKeyFreeText]);
        if (!freeTextFragments.isEmpty())
            q.addQueryItem(QStringLiteral("querytext"), QStringLiteral("\"") + freeTextFragments.join(QStringLiteral("\"+\"")) + QStringLiteral("\""));

        /// Title
        const QStringList title = OnlineSearchAbstract::splitRespectingQuotationMarks(query[queryKeyTitle]);
        if (!title.isEmpty())
            q.addQueryItem(QStringLiteral("article_title"), QStringLiteral("\"") + title.join(QStringLiteral("\"+\"")) + QStringLiteral("\""));

        /// Author
        const QStringList authors = OnlineSearchAbstract::splitRespectingQuotationMarks(query[queryKeyAuthor]);
        if (!authors.isEmpty())
            q.addQueryItem(QStringLiteral("author"), QStringLiteral("\"") + authors.join(QStringLiteral("\"+\"")) + QStringLiteral("\""));

        /// Year
        if (!query[queryKeyYear].isEmpty()) {
            q.addQueryItem(QStringLiteral("start_year"), query[queryKeyYear]);
            q.addQueryItem(QStringLiteral("end_year"), query[queryKeyYear]);
        }

        /// Sort order of results: newest publications first
        q.addQueryItem(QStringLiteral("sort_field"), QStringLiteral("publication_year"));
        q.addQueryItem(QStringLiteral("sort_order"), QStringLiteral("desc"));
        /// Request numResults many entries
        q.addQueryItem(QStringLiteral("start_record"), QStringLiteral("1"));
        q.addQueryItem(QStringLiteral("max_records"), QString::number(numResults));

        queryUrl.setQuery(q);

        return queryUrl;
    }
};

const QUrl OnlineSearchIEEEXplore::OnlineSearchIEEEXplorePrivate::apiUrl(QStringLiteral("https://ieeexploreapi.ieee.org/api/v1/search/articles?format=xml&apikey=") + InternalNetworkAccessManager::reverseObfuscate("\xea\x9b\x7e\x1f\x95\xff\x74\x5\x19\x6e\x4d\x35\x83\xf6\xee\x9a\xfc\x8b\x14\x23\x96\xf5\xb0\xdd\x98\xec\x66\x13\x82\xe1\x16\x7c\xdd\xaa\x1a\x69\x97\xef\x9c\xf7\x79\xa\x32\x51\xf6\x9e\xa1\xcf"));

OnlineSearchIEEEXplore::OnlineSearchIEEEXplore(QObject *parent)
        : OnlineSearchAbstract(parent), d(new OnlineSearchIEEEXplore::OnlineSearchIEEEXplorePrivate(this))
{
    /// nothing
}

OnlineSearchIEEEXplore::~OnlineSearchIEEEXplore()
{
    delete d;
}

void OnlineSearchIEEEXplore::startSearch(const QMap<QString, QString> &query, int numResults)
{
    m_hasBeenCanceled = false;
    emit progress(curStep = 0, numSteps = 1);

    QNetworkRequest request(d->buildQueryUrl(query, numResults));

    // FIXME 'ieeexploreapi.ieee.org' uses a SSL/TLS certificate only valid for 'mashery.com'
    // TODO re-enable certificate validation once problem has been fix (already reported)
    QSslConfiguration requestSslConfig = request.sslConfiguration();
    requestSslConfig.setPeerVerifyMode(QSslSocket::VerifyNone);
    request.setSslConfiguration(requestSslConfig);

    QNetworkReply *reply = InternalNetworkAccessManager::instance().get(request);
    InternalNetworkAccessManager::instance().setNetworkReplyTimeout(reply);
    connect(reply, &QNetworkReply::finished, this, &OnlineSearchIEEEXplore::doneFetchingXML);

    refreshBusyProperty();
}

void OnlineSearchIEEEXplore::doneFetchingXML()
{
    emit progress(++curStep, numSteps);

    QNetworkReply *reply = static_cast<QNetworkReply *>(sender());

    QUrl redirUrl;
    if (handleErrors(reply, redirUrl)) {
        if (redirUrl.isValid()) {
            /// redirection to another url
            ++numSteps;

            QNetworkRequest request(redirUrl);

            // FIXME 'ieeexploreapi.ieee.org' uses a SSL/TLS certificate only valid for 'mashery.com'
            // TODO re-enable certificate validation once problem has been fix (already reported)
            QSslConfiguration requestSslConfig = request.sslConfiguration();
            requestSslConfig.setPeerVerifyMode(QSslSocket::VerifyNone);
            request.setSslConfiguration(requestSslConfig);

            QNetworkReply *reply = InternalNetworkAccessManager::instance().get(request);
            InternalNetworkAccessManager::instance().setNetworkReplyTimeout(reply);
            connect(reply, &QNetworkReply::finished, this, &OnlineSearchIEEEXplore::doneFetchingXML);
        } else {
            /// ensure proper treatment of UTF-8 characters
            const QString xmlCode = QString::fromUtf8(reply->readAll().constData());

            /// use XSL transformation to get BibTeX document from XML result
            const QString bibTeXcode = EncoderXML::instance().decode(d->xslt.transform(xmlCode));
            if (bibTeXcode.isEmpty()) {
                qWarning() << "XSL tranformation failed for data from " << InternalNetworkAccessManager::removeApiKey(reply->url()).toDisplayString();
                stopSearch(resultInvalidArguments);
            } else {
                FileImporterBibTeX importer(this);
                File *bibtexFile = importer.fromString(bibTeXcode);

                bool hasEntries = false;
                if (bibtexFile != nullptr) {
                    for (const auto &element : const_cast<const File &>(*bibtexFile)) {
                        QSharedPointer<Entry> entry = element.dynamicCast<Entry>();
                        hasEntries |= publishEntry(entry);
                    }

                    stopSearch(resultNoError);

                    delete bibtexFile;
                } else {
                    qWarning() << "No valid BibTeX file results returned on request on" << InternalNetworkAccessManager::removeApiKey(reply->url()).toDisplayString();
                    stopSearch(resultUnspecifiedError);
                }
            }
        }
    }

    refreshBusyProperty();
}

QString OnlineSearchIEEEXplore::label() const
{
    return QStringLiteral("IEEEXplore");
}

QString OnlineSearchIEEEXplore::favIconUrl() const
{
    return QStringLiteral("http://ieeexplore.ieee.org/favicon.ico");
}

QUrl OnlineSearchIEEEXplore::homepage() const
{
    return QUrl(QStringLiteral("https://ieeexplore.ieee.org/"));
}
