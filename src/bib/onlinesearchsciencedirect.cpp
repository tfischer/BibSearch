/***************************************************************************
 *   Copyright (C) 2004-2018 by Thomas Fischer <fischer@unix-ag.uni-kl.de> *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "onlinesearchsciencedirect.h"

#include <QNetworkReply>
#include <QUrlQuery>
#include <QCoreApplication>
#include <QStandardPaths>

#ifdef HAVE_KF5
#include <KLocalizedString>
#endif // HAVE_KF5

#include "fileimporterbibtex.h"
#include "encoderxml.h"
#include "xsltransform.h"
#include "internalnetworkaccessmanager.h"

class OnlineSearchScienceDirect::OnlineSearchScienceDirectPrivate
{
public:
    static const QUrl apiUrl;
    static const QString apiKey;
    const XSLTransform xslt;

    OnlineSearchScienceDirectPrivate(OnlineSearchScienceDirect *)
            : xslt(QStandardPaths::locate(QStandardPaths::GenericDataLocation, QCoreApplication::instance()->applicationName().remove(QStringLiteral("test")) + QStringLiteral("/sciencedirectsearchapi-to-bibtex.xsl")))
    {
        /// nothing
    }

    QUrl buildQueryUrl(const QMap<QString, QString> &query, int numResults) {
        QUrl queryUrl = apiUrl;
        QUrlQuery q(queryUrl.query());

        QString queryText;

        /// Free text
        const QStringList freeTextFragments = OnlineSearchAbstract::splitRespectingQuotationMarks(query[queryKeyFreeText]);
        if (!freeTextFragments.isEmpty()) {
            if (!queryText.isEmpty()) queryText.append(QStringLiteral(" AND "));
            queryText.append(QStringLiteral("\"") + freeTextFragments.join(QStringLiteral("\" AND \"")) + QStringLiteral("\""));
        }

        /// Title
        const QStringList title = OnlineSearchAbstract::splitRespectingQuotationMarks(query[queryKeyTitle]);
        if (!title.isEmpty()) {
            if (!queryText.isEmpty()) queryText.append(QStringLiteral(" AND "));
            queryText.append(QStringLiteral("title(\"") + title.join(QStringLiteral("\" AND \"")) + QStringLiteral("\")"));
        }

        /// Authors
        const QStringList authors = OnlineSearchAbstract::splitRespectingQuotationMarks(query[queryKeyAuthor]);
        if (!authors.isEmpty()) {
            if (!queryText.isEmpty()) queryText.append(QStringLiteral(" AND "));
            queryText.append(QStringLiteral("aut(\"") + authors.join(QStringLiteral("\" AND \"")) + QStringLiteral("\")"));
        }

        q.addQueryItem(QStringLiteral("query"), queryText);

        /// Year
        if (!query[queryKeyYear].isEmpty())
            q.addQueryItem(QStringLiteral("date"), query[queryKeyYear]);

        /// Request numResults many entries
        q.addQueryItem(QStringLiteral("count"), QString::number(numResults));

        queryUrl.setQuery(q);

        return queryUrl;
    }
};

const QUrl OnlineSearchScienceDirect::OnlineSearchScienceDirectPrivate::apiUrl(QStringLiteral("https://api.elsevier.com/content/search/scidir"));
const QString OnlineSearchScienceDirect::OnlineSearchScienceDirectPrivate::apiKey(InternalNetworkAccessManager::reverseObfuscate("\x7f\x4d\xaf\x9d\x1e\x2b\x41\x75\x91\xf7\x1e\x2d\x62\x57\xa3\x92\x47\x26\xa1\x98\x4c\x7b\xd6\xb3\xc7\xa1\xb7\x84\xa6\x93\xc7\xf6\x58\x68\x51\x66\x60\x2\x3d\x59\x56\x60\x57\x6e\xde\xeb\x42\x76\xe3\x86\xec\x8f\xe\x3f\x42\x75\x5c\x6a\x71\x42\x3c\x5e\xf4\xc5"));

OnlineSearchScienceDirect::OnlineSearchScienceDirect(QObject *parent)
        : OnlineSearchAbstract(parent), d(new OnlineSearchScienceDirectPrivate(this))
{
    /// nothing
}

OnlineSearchScienceDirect::~OnlineSearchScienceDirect()
{
    delete d;
}

void OnlineSearchScienceDirect::startSearch(const QMap<QString, QString> &query, int numResults)
{
    emit progress(curStep = 0, numSteps = 1);

    QNetworkRequest request(d->buildQueryUrl(query, numResults));
    request.setRawHeader(QByteArray("X-ELS-APIKey"), d->apiKey.toLatin1());
    request.setRawHeader(QByteArray("Accept"), QByteArray("application/xml"));
    QNetworkReply *reply = InternalNetworkAccessManager::instance().get(request);
    InternalNetworkAccessManager::instance().setNetworkReplyTimeout(reply);
    connect(reply, &QNetworkReply::finished, this, &OnlineSearchScienceDirect::doneFetchingXML);

    refreshBusyProperty();
}

QString OnlineSearchScienceDirect::label() const
{
    return QStringLiteral("ScienceDirect");
}

QString OnlineSearchScienceDirect::favIconUrl() const
{
    return QStringLiteral("https://cdn.els-cdn.com/sd/favSD.ico");
}

QUrl OnlineSearchScienceDirect::homepage() const
{
    return QUrl(QStringLiteral("https://www.sciencedirect.com/"));
}

void OnlineSearchScienceDirect::doneFetchingXML()
{
    emit progress(++curStep, numSteps);

    QNetworkReply *reply = static_cast<QNetworkReply *>(sender());

    QUrl redirUrl;
    if (handleErrors(reply, redirUrl)) {
        if (redirUrl.isValid()) {
            /// redirection to another url
            ++numSteps;

            QNetworkRequest request(redirUrl);
            request.setRawHeader(QByteArray("X-ELS-APIKey"), d->apiKey.toLatin1());
            request.setRawHeader(QByteArray("Accept"), QByteArray("application/xml"));
            QNetworkReply *reply = InternalNetworkAccessManager::instance().get(request);
            InternalNetworkAccessManager::instance().setNetworkReplyTimeout(reply);
            connect(reply, &QNetworkReply::finished, this, &OnlineSearchScienceDirect::doneFetchingXML);
        } else {
            /// ensure proper treatment of UTF-8 characters
            const QString xmlCode = QString::fromUtf8(reply->readAll().constData()).remove(QStringLiteral("xmlns=\"http://www.w3.org/2005/Atom\""));

            /// use XSL transformation to get BibTeX document from XML result
            const QString bibTeXcode = EncoderXML::instance().decode(d->xslt.transform(xmlCode));
            if (bibTeXcode.isEmpty()) {
                qWarning() << "XSL tranformation failed for data from " << InternalNetworkAccessManager::removeApiKey(reply->url()).toDisplayString();
                stopSearch(resultInvalidArguments);
            } else {
                FileImporterBibTeX importer(this);
                File *bibtexFile = importer.fromString(bibTeXcode);

                bool hasEntries = false;
                if (bibtexFile != nullptr) {
                    for (const auto &element : const_cast<const File &>(*bibtexFile)) {
                        QSharedPointer<Entry> entry = element.dynamicCast<Entry>();
                        hasEntries |= publishEntry(entry);
                    }

                    stopSearch(resultNoError);

                    delete bibtexFile;
                } else {
                    qWarning() << "No valid BibTeX file results returned on request on" << InternalNetworkAccessManager::removeApiKey(reply->url()).toDisplayString();
                    stopSearch(resultUnspecifiedError);
                }
            }
        }
    }

    refreshBusyProperty();
}
