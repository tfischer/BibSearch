# BibSearch

BibSearch is a SailfishOS application which allows to search for bibliographic data on scientific publications in various catalogs and publishers such as Springer, ACM, IEEE, Google Scholar, or arXiv.

All catalogs will be searched in parallel for author, title, and free text as provided by the user.
The resulting list provides a quick overview on all matching bibliographic entries. Individual entries can be inspected for their full details.
If an entry has an URL or DOI associated, the corresponding webpage can be visited.

The core code is based on existing code derived from [KBibTeX](http://home.gna.org/kbibtex), a bibliography manager for KDE which allows you to edit and maintain your BibTeX databases.
