TARGET = harbour-bibsearch

CONFIG += sailfishapp

SOURCES += src/main.cpp \
    src/bib/bibliographymodel.cpp src/bib/value.cpp \
    src/bib/entry.cpp src/bib/macro.cpp src/bib/comment.cpp \
    src/bib/file.cpp src/bib/preamble.cpp \
    src/bib/element.cpp src/bib/internalnetworkaccessmanager.cpp \
    src/bib/onlinesearchabstract.cpp src/bib/onlinesearchbibsonomy.cpp \
    src/bib/onlinesearchacmportal.cpp src/bib/onlinesearchsciencedirect.cpp \
    src/bib/onlinesearchgooglescholar.cpp src/bib/onlinesearchjstor.cpp \
    src/bib/onlinesearchspringerlink.cpp src/bib/onlinesearchieeexplore.cpp \
    src/bib/onlinesearcharxiv.cpp src/bib/onlinesearchingentaconnect.cpp \
    src/bib/onlinesearchpubmed.cpp src/bib/encoderxml.cpp \
    src/bib/encoder.cpp src/bib/encoderlatex.cpp \
    src/bib/fileimporterbibtex.cpp src/bib/fileimporter.cpp \
    src/bib/bibtexfields.cpp src/bib/bibtexentries.cpp \
    src/bib/textencoder.cpp src/bib/xsltransform.cpp \
    src/bib/searchenginelist.cpp

HEADERS += src/bib/bibliographymodel.h \
    src/bib/entry.h src/bib/macro.h src/bib/comment.h \
    src/bib/file.h src/bib/preamble.h \
    src/bib/kbibtexnamespace.h \
    src/bib/value.h src/bib/element.h src/bib/internalnetworkaccessmanager.h \
    src/bib/onlinesearchabstract.h src/bib/onlinesearchbibsonomy.h \
    src/bib/onlinesearchacmportal.h  src/bib/onlinesearchsciencedirect.h \
    src/bib/onlinesearchgooglescholar.h src/bib/onlinesearchjstor.h \
    src/bib/onlinesearcharxiv.h src/bib/onlinesearchingentaconnect.h \
    src/bib/onlinesearchspringerlink.h src/bib/onlinesearchieeexplore.h \
    src/bib/onlinesearchpubmed.h src/bib/encoderxml.h \
    src/bib/encoder.h src/bib/encoderlatex.h \
    src/bib/fileimporter.h src/bib/fileimporterbibtex.h \
    src/bib/bibtexfields.h src/bib/bibtexentries.h \
    src/bib/textencoder.h src/bib/xsltransform.h \
    src/bib/searchenginelist.h

OTHER_FILES += qml/pages/SearchForm.qml qml/pages/EntryView.qml \
    qml/pages/AboutPage.qml qml/pages/BibliographyListView.qml \
    qml/cover/CoverPage.qml qml/BibSearch.qml \
    qml/pages/AboutPage.qml qml/pages/SearchEngineListView.qml \
    rpm/$${TARGET}.spec \
    rpm/$${TARGET}.yaml \
    translations/*.ts \
    $${TARGET}.desktop

RESOURCES += sailfishos_res.qrc

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

QT += xmlpatterns

DEFINES += KBIBTEXCONFIG_EXPORT= KBIBTEXDATA_EXPORT= KBIBTEXIO_EXPORT= KBIBTEXNETWORKING_EXPORT=

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/BibSearch-de.ts

DISTFILES += \
    qml/pages/BibliographyListView.qml \
    qml/pages/EntryView.qml \
    qml/pages/SearchForm.qml \
    qml/pages/SettingsPage.qml \
    qml/pages/AboutPage.qml

xslt.files = xslt/pam2bibtex.xsl xslt/ieeexploreapiv1-to-bibtex.xsl xslt/arxiv2bibtex.xsl \
    xslt/pubmed2bibtex.xsl xslt/sciencedirectsearchapi-to-bibtex.xsl
xslt.path = /usr/share/$${TARGET}
INSTALLS += xslt

icon108.files = icons/108/harbour-bibsearch.png
icon108.path = /usr/share/icons/hicolor/108x108/apps/
INSTALLS += icon108
icon128.files = icons/128/harbour-bibsearch.png
icon128.path = /usr/share/icons/hicolor/128x128/apps/
INSTALLS += icon128
icon256.files = icons/256/harbour-bibsearch.png
icon256.path = /usr/share/icons/hicolor/256x256/apps/
INSTALLS += icon256
